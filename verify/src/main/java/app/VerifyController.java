package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import app.exception.BadRequestException;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        try {
            String action = request.getParameter("action");
            String amount = request.getParameter("amount");
            if (action.equals("transfer")) {
                System.out.println("Verify Controller: Going to transfer $"+amount);
                RestTemplate restTemplate = new RestTemplate();
                String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
                ResponseEntity<String> response 
                    = restTemplate.getForEntity(
                            fakePaymentUrl + "?action=" + action + "&amount=" + amount, 
                            String.class);
                return response.getBody();
            } else if (action.equals("withdraw")) {
                return "Verify Controller: Sorry, you can only make transfer";
            } else {
                return "Verify Controller: You must specify action: transfer or withdraw";
            }
        } catch(RuntimeException ex) {
            throw new BadRequestException();
        }
    }

}
