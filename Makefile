APP=secdim.lab.java

all: build

test:
	docker-compose up -d payment 
	docker build --target build --tag=$(APP).verify --network $(APP).hpp --build-arg GRADLETASK=test --build-arg paymenturl=http://payment:8080/ --rm verify/
	docker-compose down

securitytest:
	docker-compose up -d payment 
	docker build --target build --tag=$(APP).verify --network $(APP).hpp --build-arg GRADLETASK=securitytest --build-arg paymenturl=http://payment:8080/ --rm verify/
	docker-compose down

build:
	docker-compose build

run:
	docker-compose up

clean:
	docker-compose down
	docker-compose rm -f
	docker system prune

.PHONY: all test securitytest clean
